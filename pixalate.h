#ifndef PIXALATE_H
#define PIXALATE_H
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QDebug>
#include <QMainWindow>

#include <QObject>
#include <QThread>
#include "colorcounter.h"
//#include "colordetectcontroller.h"

using namespace std;

class Pixalate:public QMainWindow
{

    Q_OBJECT

public slots:
    int results(int cnt);

signals:
    void setStatusProgressSignal(int val);

private:
    int bsize,blurSize,colorsCounted;


public:
    Pixalate(){
      bsize=10;
      blurSize=10;
    }
//    cv::Mat process(const cv::Mat &image);
    cv::Mat Pixalate::process(const cv::Mat &image,int xShift=0,int yShift=0);
    cv::Mat Pixalate::pixalate_minColors(const cv::Mat &image);

    void setPixelSize(int size){
        bsize=size;
    }

    void setBlurSize(int size){
        blurSize=size;
    }

    cv::Mat process_blur(const cv::Mat &image){
        cv::Mat src=image.clone();
        blur(src, src, cv::Size(blurSize, blurSize));
        return src;
    };

};
class ColorCounterController : public QObject
{
    Q_OBJECT
private:
    QThread thread;
    ColorCounter *colorCntr;
    Pixalate *pixelate;

private slots:
    void freecolorCntr(){
        delete colorCntr;
    }
public:

    //colorCntr new (mamory leak)
   ColorCounterController(const cv::Mat &image,Pixalate *pxobj) {

//        QThread* thread = new QThread;
//        colorCntr=ColorDetectController::getInstance()->colorsCountPtr();

        colorCntr= new ColorCounter();
        colorCntr->setimageThread(image);
        colorCntr->moveToThread(&thread);
        connect(&thread, SIGNAL(started()), colorCntr, SLOT(colorsCountThread()));
        connect(colorCntr, SIGNAL(finished()), &thread, SLOT(quit()));
        connect(colorCntr, SIGNAL(finished()), colorCntr, SLOT(deleteLater()));
//      connect(&thread, SIGNAL(finished()), &thread, SLOT(deleteLater()));

//            connect(colorCntr2, SIGNAL(error(QString)), this, SLOT(errorString(QString)));
//        connect(&thread, SIGNAL(finished()), colorCntr, SLOT(deleteLater()));
//        connect(colorCntr, SIGNAL(finished()), this, SLOT(freecolorCntr()));//free mem
//        connect(&thread, SIGNAL(finished()), this, SLOT(freecolorCntr()));//free mem

        connect(colorCntr, SIGNAL(results(int)), pxobj, SLOT(results(int)));
        thread.start();
    }


   //colorCntr from parent
   ColorCounterController(const cv::Mat &image, ColorCounter *clrCntr,Pixalate *pxobj) {

    colorCntr=clrCntr;
    pixelate=pxobj;

    colorCntr->setimageThread(image);
    colorCntr->moveToThread(&thread);
    connect(&thread, SIGNAL(started()), colorCntr, SLOT(colorsCountThread()));
    connect(colorCntr, SIGNAL(finished()), &thread, SLOT(quit()));
    connect(colorCntr, SIGNAL(finished()), colorCntr, SLOT(deleteLater()));

    connect(colorCntr, SIGNAL(results(int)), pxobj, SLOT(results(int)));
    thread.start();

   }

   //via General Controller
   ColorCounterController( ColorCounter *clrCntr,Pixalate *pxobj) {
    colorCntr=clrCntr;
    pixelate=pxobj;
   }

   void doit(const cv::Mat &image){
       colorCntr->setimageThread(image);
       colorCntr->moveToThread(&thread);
       connect(&thread, SIGNAL(started()), colorCntr, SLOT(colorsCountThread()));
       connect(colorCntr, SIGNAL(finished()), &thread, SLOT(quit()));
       connect(colorCntr, SIGNAL(finished()), colorCntr, SLOT(deleteLater()));
       connect(colorCntr, SIGNAL(results(int)), pixelate, SLOT(results(int)));
       thread.start();
   }


    ~ColorCounterController() {
        thread.quit();
        thread.wait();
         qDebug() << QString("Controller quit wait");
//        delete colorCntr; //err
    }

};
#endif // PIXALATE_H
