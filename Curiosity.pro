#-------------------------------------------------
#
# Project created by QtCreator 2015-07-11T18:07:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Curiosity
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    colordetectcontroller.cpp \
    colordetector.cpp \
    qpicturelabel.cpp \
    clickqlable.cpp \
    pixalate.cpp \
    colorizer.cpp \
    colorcounter.cpp

HEADERS  += mainwindow.h \
    colordetectcontroller.h \
    colordetector.h \
    qpicturelabel.h \
    clickqlable.h \
    pixalate.h \
    colorcounter.h \
    colorizer.h

FORMS    += mainwindow.ui

INCLUDEPATH += Z:/opencv/build/include
LIBS +=     -lZ:/opencv/build/x64/vc12/lib/opencv_world300 \
            -lZ:/opencv/build/x64/vc12/lib/opencv_ts300

RESOURCES += \
    images.qrc
