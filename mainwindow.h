#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QColorDialog>
#include <QMessageBox>
#include <QDebug>
#include <QtGui>
#include <QtCore>
#include <QProgressBar>
#include <QStandardItemModel>
#include <QStandardItem>
//OpenCV
//#include <opencv2/core/core.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/highgui/highgui.hpp>

//color detector, controller
//#include "colorDetectController.h"
//#include "colordetector.h"
//#include "pixalate.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent* event);

private:
    Ui::MainWindow *ui;
    QColor btn_color;
    QColor color;
    QProgressBar *statusProgress;
    void resultsToLabel();

//public slots:

private slots:

    void setStatusProgress(int val);
    void processColorDetection();
    void on_verticalSlider_Threshold_valueChanged(int value);

    void setColor();
    void setImage();
    void reset_img_slot();
    void on_pushButton_pixalate_clicked();
    void on_spinBox_pixalate_valueChanged(int arg1);
    void on_pushButton_reduce_color_64_clicked();
    void on_spinBox_reduce_color_valueChanged(int arg1);
    void on_pushButton_reduce_color_clicked();
    void on_pushButton_kmeans_clicked();
    void on_pushButton_blur_clicked();
    void on_pushButton_colors_to_HSV_clicked();
    void on_pushButton_colors_to_Gray_clicked();

    void showColorsCountInStatusBar();
    void on_pushButton_pixalate_minColors_clicked();
};

#endif // MAINWINDOW_H
