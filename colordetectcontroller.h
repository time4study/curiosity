#if !defined CD_CNTRLLR
#define CD_CNTRLLR

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "colordetector.h"
#include "pixalate.h"
#include "colorizer.h"
#include "colorcounter.h"

class MainController {

  private:

    static MainController *singleton; // pointer to the singleton

    ColorDetector *cdetect;
    Colorizer *colorizer;
    ColorCounter *colorCntr;
    ColorCounterController *colorCntrController;

    Pixalate *pixalate;

    // The image to be processed
    cv::Mat image;
    cv::Mat result;
    cv::Mat intermediate;


  public:
    MainController() { // private constructor
          //setting up the application
          cdetect= new ColorDetector();
          pixalate= new Pixalate();
          colorizer= new Colorizer();
          colorCntr= new ColorCounter();
          colorCntrController =new ColorCounterController(colorCntr,pixalate);
    }

      Pixalate *returnPixalateObj(){
          return pixalate;
      }

      void doit(const cv::Mat &img){
          colorCntrController->doit(img);
      }

      // Sets the color distance threshold
      void setColorDistanceThreshold(int distance)
      {
          cdetect->setColorDistanceThreshold(distance);
      }

      // Gets the color distance threshold
      int getColorDistanceThreshold() const
      {
          return cdetect->getColorDistanceThreshold();
      }

      // Sets the color to be detected
      void setTargetColor(unsigned char red, unsigned char green, unsigned char blue)
      {
          cdetect->setTargetColor(red,green,blue);
      }

      void setPixelSize(int size){
          pixalate->setPixelSize(size);
      }

      // Gets the color to be detected
      void getTargetColor(unsigned char &red, unsigned char &green, unsigned char &blue) const {

          cv::Vec3b color= cdetect->getTargetColor();

          red= color[2];
          green= color[1];
          blue= color[0];
      }

      // Sets the input image. Reads it from file.
      bool setInputImage(std::string filename) {

          image= cv::imread(filename);
          intermediate=image.clone();
          if (!image.data)
              return false;
          else
              return true;
      }

      // Returns the current input image.
      const cv::Mat getInputImage() const {
          return image;
      }

      const void resetResult(){
          result=image.clone();
          intermediate=image.clone();
      }

      // Performs image processing.
      void process() {
          if(!intermediate.empty())
              result= cdetect->process(intermediate);
          else if(!image.empty())
              result= cdetect->process(image);
      }

      // Performs image pixalating.
      void process_pixalate() {
          if(!result.empty())
              result= pixalate->process(result);
          else if(!image.empty())
              result= pixalate->process(image);
          intermediate=result.clone();
      }

      // Performs image pixalating.
      void process_pixalate_minColors() {
          if(!result.empty())
              result= pixalate->pixalate_minColors(result);
          else if(!image.empty())
              result= pixalate->pixalate_minColors(image);
          intermediate=result.clone();
      }

      // Performs color reducing to 64.
      void process_color_reducing_64() {
          if(!result.empty())
              result= colorizer->reduceColors64(result);
          else if(!image.empty())
              result= colorizer->reduceColors64(image);
          intermediate=result.clone();
      }

      // Performs color reducing
      void process_color_reducing() {
          if(!result.empty())
              result= colorizer->reduceColors(result);
          else if(!image.empty())
              result= colorizer->reduceColors(image);
          intermediate=result.clone();
      }
      void set_colors_amount(int amount){
          colorizer->setColorsAmount(amount);
      }

      void process_kmeans(){
          if(!result.empty())
              result= colorizer->k_means(result);
          else if(!image.empty())
              result= colorizer->k_means(image);
          intermediate=result.clone();
      }
      void set_kmeans_parameters(int int_attempts,int int_clusterCount,int int_iterations,double double_epsilon){
          colorizer->setKmeansParameters(int_attempts,int_clusterCount,int_iterations,double_epsilon);
      }

       void process_blur(){
           if(!result.empty())
               result= pixalate->process_blur(result);
           else if(!image.empty())
               result= pixalate->process_blur(image);
           intermediate=result.clone();
       }

    void setBlurSize(int size){
        pixalate->setBlurSize(size);
    }

    int colorsCount(){
        if(!result.empty())
            return colorCntr->colorsCount(result);
        else if(!image.empty())
            return colorCntr->colorsCount(image);
    }

    int colorsCount(const cv::Mat &img){
        return colorCntr->colorsCount(img);
    }

    void process_BGR2HSV(){
        if(!result.empty())
            result= colorizer->BGR2HSV(result);
        else if(!image.empty())
            result= colorizer->BGR2HSV(image);
        intermediate=result.clone();
    }

    void process_BGR2GRAY(){
        if(!result.empty())
            result= colorizer->BGR2GRAY(result);
        else if(!image.empty())
            result= colorizer->BGR2GRAY(image);
        intermediate=result.clone();
    }

    // Returns the image result from the latest processing.
      const cv::Mat getLastResult() const {
          return result;
      }

      // Deletes all processor objects created by the controller.
      ~MainController() {
          delete cdetect;
          delete pixalate;
          delete colorizer;
      }

      // Singleton static members
      static MainController *getInstance() {
          if (singleton == 0)
            singleton= new MainController;
          return singleton;
      }

      // Releases the singleton instance of this controller.
      static void destroy() {
          if (singleton != 0) {
              delete singleton;
              singleton= 0;
          }
      }
};

#endif
