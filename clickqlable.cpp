#include "clickqlable.h"
#include "mainwindow.h"

clickQlable::clickQlable( QWidget * parent)
    :QLabel(parent)
{
    connect(this,SIGNAL(Mouse_Left()),this,SLOT(mouse_left()));
    connect(this,SIGNAL(Mouse_LB_Pressed_signal()),this,SLOT(mouse_LB_pressed_slot()));
    connect(this,SIGNAL(Mouse_Pos()),this,SLOT(mouse_current_pos()));

    connect(this,SIGNAL(Mouse_MB_Pressed_signal()),this,SLOT(mouse_MB_pressed_slot()));
    connect(this,SIGNAL(Mouse_RB_Pressed_signal()),this,SLOT(mouse_RB_pressed_slot()));
    connect(this,SIGNAL(Mouse_LB_Released_signal()),this,SLOT(mouse_LB_released_slot()));
    connect(this,SIGNAL(Mouse_MB_Released_signal()),this,SLOT(mouse_MB_released_slot()));
    connect(this,SIGNAL(Mouse_RB_Released_signal()),this,SLOT(mouse_RB_released_slot()));

    QString fileName=QString("z:/Google/PC_STUDIA/StoreUpdateBooster/jpg/859-5.jpg");
       if (!fileName.isEmpty()){
           img_mat = cv::imread(fileName.toStdString(),1); //0 for grayscale
           displayMat(img_mat);
       }
    MainController::getInstance()->setInputImage(fileName.toStdString());
}

void clickQlable::mouseMoveEvent(QMouseEvent *ev)
{
    this->x=ev->x();
    this->y=ev->y();
//    if((ev->button()== Qt::LeftButton))
        emit Mouse_Pos();
}

void clickQlable::mousePressEvent(QMouseEvent *ev)
{
    this->x=ev->x();
    this->y=ev->y();
    if(ev->button()== Qt::LeftButton)
        emit Mouse_LB_Pressed_signal();
    if(ev->button()== Qt::MiddleButton)
        emit Mouse_MB_Pressed_signal();
    if(ev->button()== Qt::RightButton)
        emit Mouse_RB_Pressed_signal();
}

void clickQlable::leaveEvent(QEvent *)
{
    emit Mouse_Left();
}

void clickQlable::mouseReleaseEvent(QMouseEvent *ev)
{
    if(ev->button()== Qt::LeftButton)
        emit Mouse_LB_Released_signal();
    if(ev->button()== Qt::MiddleButton)
        emit Mouse_MB_Released_signal();
    if(ev->button()== Qt::RightButton)
        emit Mouse_RB_Released_signal();
}

void clickQlable::mouse_left()
{
    qDebug() << QString("Mouse Left!");
}

void clickQlable::mouse_current_pos()
{
    qDebug() << QString("X = %1, Y = %2")
               .arg(this->x)
               .arg(this->y);
    scale_image();
}

void clickQlable::mouse_LB_pressed_slot()
{
    qDebug() << QString("Mouse LB Pressed!");
    scale_image();
}

void clickQlable::mouse_RB_pressed_slot()
{
    qDebug() << QString("Mouse RB Pressed!");
    if (!img_mat.empty())
        displayMat(img_mat);
//    if (!img_pix_src.isNull())
//        this->ui->label->setPixmap(img_pix_src.scaled(ui->label->size(), Qt::KeepAspectRatio));

}

void clickQlable::mouse_RB_released_slot()
{
    qDebug() << QString("Mouse RB Released!");
    if (!resulting.empty())
        displayMat(resulting);
    else
        displayMat(img_mat);
//    if (!img_pix.isNull())
//        this->ui->label->setPixmap(img_pix.scaled(ui->label->size(), Qt::KeepAspectRatio));
}

void clickQlable::mouse_MB_pressed_slot()
{
    qDebug() << QString("Mouse MB Pressed!");
}
void clickQlable::mouse_MB_released_slot()
{
    qDebug() << QString("Mouse MB Released!");
}


void clickQlable::mouse_LB_released_slot()
{
    qDebug() << QString("Mouse LB Released!");
    this->setPixmap(img_pix.scaled(this->size(), Qt::KeepAspectRatio));
}

void clickQlable::scale_image(){
    float h= this->height();
    float w=this->width();
    float w_src=img_pix.width();
//    float h_src=img_pix.height();
    float wscale=w_src/w;
//    float hscale=h_src/h;
    QPixmap img_pix_cpy;//=img_pix.copy(img_pix.rect());

    float x=(float)this->x;
    float y=(float)this->y;
    img_pix_cpy=img_pix.copy(x*wscale-w/2,y*wscale-h/2,w,h);
    this->setPixmap(img_pix_cpy.scaled(this->size()*2, Qt::KeepAspectRatio));
}

//Convert cv::Mat to QImage and display
void clickQlable::displayMat(const cv::Mat& image){

    //BGR openCV Mat to QImage
    QImage img_qt = QImage((const unsigned char*)(image.data),image.cols, image.rows, (int)image.step, QImage::Format_RGB888);
    //For Binary Images
    if (img_qt.isNull()){
        //ColorTable for Binary Images
        QVector<QRgb> colorTable;
        for (int i = 0; i < 256; i++)
            colorTable.push_back(qRgb(i, i, i));
        img_qt = QImage((const unsigned char*)image.data,image.cols, image.rows,(int)image.step, QImage::Format_Indexed8);
        img_qt.setColorTable(colorTable);
    }

    //Display the QImage in the Label
//    QPixmap img_pix = QPixmap::fromImage(img_qt.rgbSwapped()); //BGR to RGB
    if (img_pix_src.isNull()) img_pix_src=img_pix;

    img_pix = QPixmap::fromImage(img_qt.rgbSwapped()); //BGR to RGB

    this->setPixmap(img_pix.scaled(this->size(), Qt::KeepAspectRatio));
}


