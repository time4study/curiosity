#include "pixalate.h"
#include "colorcounter.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace cv;


cv::Mat Pixalate::process(const cv::Mat &image,int xShift,int yShift)
{
    cv::Mat dst = Mat::zeros(image.size(), CV_8UC3);

    for (int i = 0+yShift; i < image.rows; i += bsize)
    {
        for (int j = 0+xShift; j < image.cols; j += bsize)
        {
//            if ((xShift!=0 || yShift!=0)){
//                Rect empty_rect= Rect(j, i,  bsize-xShift, bsize-yShift) & Rect(0, 0, image.cols, image.rows);
//                cv::Mat sub_dst(dst, empty_rect);
//            }
            Rect rect = Rect(j, i, bsize, bsize) & Rect(0, 0, image.cols, image.rows);
            cv::Mat sub_dst(dst, rect);
            sub_dst.setTo(mean(image(rect)));
        }
    }
    return dst;
};

cv::Mat Pixalate::pixalate_minColors(const cv::Mat &image)
{
    int box=bsize;
    cv::Mat src = process(image);

    int minColors=MainController::getInstance()->colorsCount(src);

    int minx=0,miny=0;
    float cnt=0;
    int cnt2=0;
    float amnt=box;
    int colors=minColors;

    const int arrSize=8;
    ColorCounterController *cntrl[arrSize];
    for (int i = 0; i < arrSize; i++){
         cntrl[i]=NULL;
    }

    QTime time;
    time.start();

//    ColorCounter *colorCntr;//= new ColorCounter();

    ColorCounter *colorCntr= new ColorCounter();

    for (int i = 0; i < box; i++)//x
    {
        for (int j = 0; j < box; ++j)//y
        {
            cv::Mat res=process(image,i,j);//image of interest
            //Using 1 core
//            colors=ColorDetectController::getInstance()->colorsCount(res);

            //Using all 8 cores
            cntrl[cnt2%arrSize]= new ColorCounterController(res,this);
//            cntrl[cnt2%arrSize]= new ColorCounterController(res,colorCntr,this);
//            ColorDetectController::getInstance()->doit(res);


            //            colors=colorsCounted;

            if (colors<minColors){
                minColors=colors;
                minx=i;
                miny=j;
            }
            qDebug() << QString ("min_colors=%1, colors %2, i=%3, j=%4").arg(minColors).arg(colors).arg(i).arg(j);
            ++cnt2;
        }

        ++cnt;
        emit setStatusProgressSignal((int)(cnt/amnt*100));
    }

//    delete[] *cntrl;

    for (int i = 0; i < arrSize; i++){
//        if (cntrl[i])
            delete cntrl[i];
    }

    delete colorCntr;

    int difference = time.elapsed();
     qDebug() <<QString ("time.elapsed()=%1").arg(difference);

//    delete[] cntrl;

    cv::Mat dst=process(image,minx,miny);
    return dst;
};

int Pixalate::results(int cnt)
{
   qDebug() << QString ("THREAD min_colors=%1").arg(cnt);
   colorsCounted=cnt;
   return cnt;
}


