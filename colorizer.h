#ifndef COLORIZER_H
#define COLORIZER_H
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <QDebug>
#include <QSet>
#include <unordered_set>
//using namespace cv;
//using namespace std;

#include <vector>
#include <random>
#include <iostream>
//#include <boost/chrono.hpp>

class Colorizer
{
public:
    Colorizer(){
        div=10;
        attempts=3;
        clusterCount=10;
        iterations=10000;
        epsilon=0.0001;
    };


    void setColorsAmount(int colors){
        div=  colors;
    }

    void setKmeansParameters (int int_attempts,int int_clusterCount,int int_iterations,double double_epsilon){
        attempts=int_attempts;
        clusterCount=int_clusterCount;
        iterations=int_iterations;
        epsilon=double_epsilon;
    }


cv::Mat k_means(const cv::Mat &image)//http://stackoverflow.com/questions/10240912/input-matrix-to-opencv-kmeans-clustering
{
        //Mat src = imread(argv[1], 1);
    cv::Mat src=image.clone();

    cv::Mat samples(src.rows * src.cols, 3, CV_32F);
    for (int y = 0; y < src.rows; y++)
        for (int x = 0; x < src.cols; x++)
            for (int z = 0; z < 3; z++)
                samples.at<float>(y + x*src.rows, z) = src.at<cv::Vec3b>(y, x)[z];



            cv::Mat labels;

            cv::Mat centers;
            kmeans(samples, clusterCount, labels, cv::TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, iterations, epsilon), attempts, cv::KMEANS_PP_CENTERS, centers);


            cv::Mat new_image(src.size(), src.type());
            for (int y = 0; y < src.rows; y++)
                for (int x = 0; x < src.cols; x++)
                {
                    int cluster_idx = labels.at<int>(y + x*src.rows, 0);
                    new_image.at<cv::Vec3b>(y, x)[0] = centers.at<float>(cluster_idx, 0);
                    new_image.at<cv::Vec3b>(y, x)[1] = centers.at<float>(cluster_idx, 1);
                    new_image.at<cv::Vec3b>(y, x)[2] = centers.at<float>(cluster_idx, 2);
                }

                return new_image;
            }


cv::Mat reduceColors(const cv::Mat &image) {
    cv::Mat res=image.clone();
    int nl = image.rows; // number of lines
    // total number of elements per line
    int nc = image.cols * image.channels();

    for (int j = 0; j < nl; j++) {
        // get the address of row j
        uchar* data = res.ptr<uchar>(j);
        for (int i = 0; i < nc; i++) {
            // process each pixel ---------------------

            data[i] = data[i] / div*div + div / 2;
            // end of pixel processing ----------------
        } // end of line
    }
    return res;
}

cv::Mat reduceColors64(const cv::Mat &img)
{
    cv::Mat res=img.clone();
    uchar* pixelPtr = res.data;
    for (int i = 0; i < img.rows; i++)
    {
        for (int j = 0; j < img.cols; j++)
        {
            const int pi = i*img.cols * 3 + j * 3;
            pixelPtr[pi + 0] = reduceVal(pixelPtr[pi + 0]); // B
            pixelPtr[pi + 1] = reduceVal(pixelPtr[pi + 1]); // G
            pixelPtr[pi + 2] = reduceVal(pixelPtr[pi + 2]); // R
        }
    }
    return res;
}

cv::Mat BGR2HSV(const cv::Mat &img)
{
    cv::Mat res=img.clone();
    cv::cvtColor(res,res,CV_BGR2HSV);
    return res;
}

cv::Mat BGR2GRAY(const cv::Mat &img)
{
    cv::Mat res;
    cv::cvtColor(img,res,CV_BGR2GRAY);
    cv::cvtColor(res, res, CV_GRAY2RGB);
    return res;
}

private:

//k-means
    int attempts,clusterCount, iterations, epsilon;
//k-means END

    int div;

    inline uchar reduceVal(const uchar val) //http://stackoverflow.com/questions/5906693/how-to-reduce-the-number-of-colors-in-an-image-with-opencv-in-python
    {
        if (val < 64) return 0;
        if (val < 128) return 64;
        return 255;
    }


};

#endif // COLORIZER_H
