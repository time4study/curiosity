#ifndef CLICK_QLABLE_H
#define CLICK_QLABLE_H

#include <QWidget>
#include <QLabel>
#include <QEvent>
#include <QMouseEvent>
#include <QDebug>
#include "colorDetectController.h"
#include "colordetector.h"

class clickQlable : public QLabel
{
    Q_OBJECT
public:
   clickQlable(QWidget * parent = 0);
   ~clickQlable(){}

   void mouseMoveEvent(QMouseEvent *ev);
   void mousePressEvent(QMouseEvent *ev);
   void leaveEvent(QEvent *);
   void mouseReleaseEvent(QMouseEvent *ev);
   void displayMat(const cv::Mat& img);
   int x,y;
   QPixmap img_pix;
   QPixmap img_pix_src;

   cv::Mat resulting;
   cv::Mat img_mat;

signals:
   void Mouse_LB_Pressed_signal();
   void Mouse_MB_Pressed_signal();
   void Mouse_RB_Pressed_signal();
   void Mouse_Pos();
   void Mouse_Left();
   void Mouse_RB_Released_signal();
   void Mouse_MB_Released_signal();
   void Mouse_LB_Released_signal();

private:
   void scale_image();


private slots:
   void mouse_left();
   void mouse_LB_pressed_slot();
   void mouse_current_pos();

   void mouse_RB_pressed_slot();
   void mouse_MB_pressed_slot();
   void mouse_RB_released_slot();
   void mouse_MB_released_slot();
   void mouse_LB_released_slot();
};

#endif // CLICK_QLABLE_H
