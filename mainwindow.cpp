#include "mainwindow.h"
#include "ui_mainwindow.h"

//#include "pixalate.h"
// #include "qpicturelabel.h" //not used yet
MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    btn_color=Qt::white;
    ui->pushButton_color->setStyleSheet(QString("background-color: %1").arg(btn_color.name()));

    //select color
    connect(ui->pushButton_color, SIGNAL(clicked()), this, SLOT(setColor()));

    //open image
    connect(ui->pushButton_openImage, SIGNAL(clicked()), this, SLOT(setImage()));

    //process Color Detection
    connect(ui->pushButton_process, SIGNAL(clicked()), this, SLOT(processColorDetection()));

    //process on_pushButton_pixalate_clicked
    //connect(ui->pushButton_pixalate, SIGNAL(clicked()), this, SLOT(on_pushButton_pixalate_clicked()));

    //Reset img
    connect(ui->pushButton_reset, SIGNAL(clicked()), this, SLOT(reset_img_slot()));

    connect(MainController::getInstance()->returnPixalateObj(), SIGNAL(setStatusProgressSignal(int)), this, SLOT(setStatusProgress(int)));


    showColorsCountInStatusBar();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setStatusProgress(int val)
{
    statusProgress->setValue(val);
    if (val==100){
        delete statusProgress;
    }
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
        default:
        break;
    }
}

void MainWindow::setImage()
{
    QFileDialog::Options options;
    QString selectedFilter;
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Image Files"),
        tr("z:/Google/PC_STUDIA/StoreUpdateBooster/jpg/"),
        tr("Image files (*.jpg *.jpeg *.png *.gif *.bmp)"),
        &selectedFilter,
        options);
// QString fileName=QString("z:/Google/PC_STUDIA/StoreUpdateBooster/jpg/869.jpg");
    if (!fileName.isEmpty()){
        ui->label->img_mat = cv::imread(fileName.toStdString(),1); //0 for grayscale
        ui->label->resulting = ui->label->img_mat;
        ui->label->displayMat(ui->label->img_mat);
    }
    //Set Filename
    MainController::getInstance()->setInputImage(fileName.toStdString());
    MainController::getInstance()->resetResult();
    showColorsCountInStatusBar();
}

void MainWindow::reset_img_slot()
{
    qDebug() << QString("reset_img_slot");
    ui->label->displayMat(ui->label->img_mat);
    ui->label->resulting = ui->label->img_mat;
    MainController::getInstance()->resetResult();
    showColorsCountInStatusBar();
}

void MainWindow::on_verticalSlider_Threshold_valueChanged(int value)
{
    QString cdt("Color Distance Threshold: ");
    cdt.append(QString::number(value));
    ui->label_2->setText(cdt);
    processColorDetection();
    showColorsCountInStatusBar();
}

void MainWindow::setColor()
{
    color = QColorDialog::getColor(btn_color, this);
    if (color.isValid()) {
        MainController::getInstance()->setTargetColor(color.red(),color.green(),color.blue());
        ui->pushButton_color->setStyleSheet(QString("background-color: %1").arg(color.name()));
        processColorDetection();
    }
}

void MainWindow::processColorDetection()
{
    if(!color.isValid())
        MainController::getInstance()->setTargetColor(btn_color.red(),btn_color.green(),btn_color.blue());
    MainController::getInstance()->setColorDistanceThreshold(ui->verticalSlider_Threshold->value());
    MainController::getInstance()->process();
    resultsToLabel();
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    QMainWindow::resizeEvent(event);
    if(!ui->label->img_pix.isNull()){
        ui->label->setPixmap(ui->label->img_pix.scaled(ui->label->size(), Qt::KeepAspectRatio,Qt::FastTransformation));
    }
}

void MainWindow::on_pushButton_pixalate_minColors_clicked()
{
    qDebug() << QString ("on_pushButton_pixalate_minColors_clicked");
    MainController::getInstance()->setPixelSize(ui->spinBox_pixalate->value());
    statusProgress = new QProgressBar(this);
    ui->statusBar->addPermanentWidget(statusProgress);
    statusProgress->setTextVisible(false);
    statusProgress->setVisible(true);
    MainController::getInstance()->process_pixalate_minColors();
    resultsToLabel();
}

void MainWindow::on_pushButton_pixalate_clicked()
{
    qDebug() << QString ("on_pushButton_pixalate_clicked");
    MainController::getInstance()->setPixelSize(ui->spinBox_pixalate->value());
    MainController::getInstance()->process_pixalate();
    resultsToLabel();
}

void MainWindow::on_spinBox_pixalate_valueChanged(int size)
{
    MainController::getInstance()->setPixelSize(size);
}

void MainWindow::on_pushButton_reduce_color_64_clicked()
{
    MainController::getInstance()->process_color_reducing_64();
    resultsToLabel();

}

void MainWindow::on_spinBox_reduce_color_valueChanged(int amount)
{
    MainController::getInstance()->set_colors_amount(amount);
}

void MainWindow::on_pushButton_reduce_color_clicked()
{
    qDebug() << QString ("on_pushButton_reduce_color_clicked");
    MainController::getInstance()->set_colors_amount(ui->spinBox_reduce_color->value());
    MainController::getInstance()->process_color_reducing();
    resultsToLabel();
}

void MainWindow::on_pushButton_kmeans_clicked()
{
    qDebug() << QString ("on_pushButton_kmeans_clicked");
    MainController::getInstance()->set_kmeans_parameters(
        ui->spinBox_kmeans_attempts->value(),
        ui->spinBox_kmeans_clusters->value(),
        ui->spinBox_kmeans_iterations->value(),
        ui->doubleSpinBox_kmeans_epsilon->value()
        );
    MainController::getInstance()->process_kmeans();
    resultsToLabel();
}

void MainWindow::on_pushButton_blur_clicked()
{
    MainController::getInstance()->setBlurSize(ui->spinBox_blur->value());
    MainController::getInstance()->process_blur();
    resultsToLabel();
}


void MainWindow::on_pushButton_colors_to_HSV_clicked()
{
    qDebug() << QString ("on_pushButton_colors_to_HSV_clicked");
    MainController::getInstance()->process_BGR2HSV();
    resultsToLabel();
}

void MainWindow::on_pushButton_colors_to_Gray_clicked()
{
    qDebug() << QString ("on_pushButton_colors_to_Gray_clicked");
    MainController::getInstance()->process_BGR2GRAY();
    resultsToLabel();
}
void MainWindow::resultsToLabel()
{
    ui->label->resulting = MainController::getInstance()->getLastResult();
    if (!ui->label->resulting.empty())
        ui->label->displayMat(ui->label->resulting);
    showColorsCountInStatusBar();
}

void MainWindow::showColorsCountInStatusBar(){
    ui->statusBar->showMessage(QString("%1 colors").arg(MainController::getInstance()->colorsCount()));
}

