#ifndef COLORCOUNTER_H
#define COLORCOUNTER_H

//#include <QtCore>
#include <QObject>
//#include <QDebug>

#include <vector>
#include <random>
#include <iostream>
//#include <boost/chrono.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#define  _16M 256*256*256

class ColorCounter: public QObject
{
    Q_OBJECT

signals:
    void finished();
    void results(int x);

public slots:
//    void process();
    void colorsCountThread();

private:
    cv::Mat imageThread;

public:
    ColorCounter(){};

    void setimageThread(const cv::Mat &image){
        imageThread=image;//.clone();
    }

    ~ColorCounter(){
       imageThread.deallocate();
    };

    typedef union {
        struct { unsigned char r,g,b,n ; } r_g_b_n ;
        unsigned char rgb[4] ;
        unsigned i_rgb;
    } RGB ;

    RGB make_RGB(unsigned char r, unsigned char g , unsigned char b) {
        RGB res;
        res.r_g_b_n.r = r;
        res.r_g_b_n.g = g;
        res.r_g_b_n.b = b;
        res.r_g_b_n.n = 0;
        return res;
    }

    static_assert(sizeof(RGB)==4,"bad RGB size not 4");
    static_assert(sizeof(unsigned)==4,"bad i_RGB size not 4");
    struct Image;
    void FillRandom(Image & im);
    void convertToMat(Image & im, const cv::Mat &image);
    int Rc(const cv::Vec3b& color) const {return color[2];}
    int Gc(const cv::Vec3b& color) const {return color[1];}
    int Bc(const cv::Vec3b& color) const {return color[0];}
    size_t Count(const Image & im);
    int colorsCount(const cv::Mat &image);

//    int colorsCount(const cv::Mat &image){
//        cv::Mat clone=image.clone();

//        cv::Mat_<cv::Vec3b>::iterator it= clone.begin<cv::Vec3b>();
//        cv::Mat_<cv::Vec3b>::iterator itend= clone.end<cv::Vec3b>();

//  // get the iterators
////  cv::Mat_<uchar>::iterator itout= result.begin<uchar>();
//        QSet<QString> set;
////        std::unordered_set<std::string> myset = { "red","green","blue" };
//        std::unordered_set<std::string> myset;
//    // for each pixel
////        for (int i=0; i<image.cols * image.channels(); i++) {
//        for ( ; it!= itend; ++it) {
//      // process each pixel ---------------------

////            set.insert(getColorsString(*it));
//            myset.insert(getColorsString(*it));
//      // end of pixel processing ----------------
//        }
//        return (int)myset.size();
////        return set.size();
//    }




//    QString getColorsString(const cv::Vec3b& color) const {
//        return QString("%1_%2_%3").arg(color[0]).arg(color[1]).arg(color[2]);
//    }
//	std::string getColorsString(const cv::Vec3b& color) const {
//		return std::to_string(color[0])+"_"+std::to_string(color[1])+"_"+std::to_string(color[2]);
//	}



};

#endif // COLORCOUNTER_H
