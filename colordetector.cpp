#include "colordetector.h"
using namespace std;
cv::Mat ColorDetector::process(const cv::Mat &image) {

      // re-allocate binary map if necessary
      // same size as input image, but 1-channel
//    cv::Mat g;
        if (image.channels()==1) {
//            g = cv::Mat::zeros(image.rows, image.cols, image.type());
//            vector<cv::Mat> channels;
//            channels.push_back(g);
//            channels.push_back(g);
//            channels.push_back(g);
//            merge(channels, converted);
//            cv::cvtColor(image, converted, CV_8UC3);
            return image;
        }else{
            // re-allocate intermediate image if necessary
            converted.create(image.rows,image.cols,image.type());
        }
      result.create(image.rows,image.cols,CV_8U);


      // Converting to Lab color space
      cv::cvtColor(image, converted, CV_BGR2Lab);

      // get the iterators
      cv::Mat_<cv::Vec3b>::iterator it= converted.begin<cv::Vec3b>();
      cv::Mat_<cv::Vec3b>::iterator itend= converted.end<cv::Vec3b>();
      cv::Mat_<uchar>::iterator itout= result.begin<uchar>();

      // for each pixel
      for ( ; it!= itend; ++it, ++itout) {

        // process each pixel ---------------------

          // compute distance from target color
          if (getDistance(*it)<minDist) {

              *itout= 255;

          } else {

              *itout= 0;
          }

        // end of pixel processing ----------------
      }
      cv::cvtColor(result, result, CV_GRAY2RGB);
      return result;
}
