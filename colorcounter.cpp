#include "colorcounter.h"
//http://stackoverflow.com/questions/14769841/faster-algorithm-to-check-the-colors-in-a-image

//#include <QThread>

struct ColorCounter::Image
{
    Image (unsigned M, unsigned N) : M_(M) , N_(N) , v_(M*N) {}
    const RGB* tab() const {return & v_[0] ; }
    RGB* tab() {return & v_[0] ; }
    unsigned M_ , N_;
    std::vector<RGB> v_;
};



void ColorCounter::FillRandom(Image & im) {
    std::uniform_int_distribution<unsigned> rnd(0,_16M-1);
    std::mt19937 rng;
    const int N = im.M_ * im.N_;
    RGB* tab = im.tab();
    for (int i=0; i<N; i++) {
        unsigned r = rnd(rng) ;
        *tab++ = make_RGB(  (r & 0xFF) , (r>>8 & 0xFF), (r>>16 & 0xFF) ) ;
    }
}

void ColorCounter::convertToMat(Image & im, const cv::Mat &image) {

//    const int N = im.M_ * im.N_;
    RGB* tab = im.tab();
    cv::Mat clone=image.clone();


    cv::Mat_<cv::Vec3b>::iterator it= clone.begin<cv::Vec3b>();
    cv::Mat_<cv::Vec3b>::iterator itend= clone.end<cv::Vec3b>();
    unsigned R_,G_,B_;
    for ( ; it!= itend; ++it) {
  // process each pixel ---------------------

//            set.insert(getColorsString(*it));
//            myset.insert(getColorsString(*it));
        R_=Rc(*it);
        G_=Gc(*it);
        B_=Bc(*it);
      // end of pixel processing ----------------
        *tab++ = make_RGB( R_ , G_ , B_ ) ;
    }
}

size_t ColorCounter::Count(const Image & im) {
    const int N = im.M_ * im.N_;
    std::vector<char> count(_16M,0);
    const RGB* tab = im.tab();
#pragma omp parallel
    {
#pragma omp for
        for (int i=0; i<N; i++) {
            count[ tab->i_rgb ] = 1 ;
            tab++;
        }
    }
    size_t nColors = 0 ;
#pragma omp parallel
    {
#pragma omp for
        for (int i = 0 ; i<_16M; i++) nColors += count[i];
    }
return nColors;
}

int ColorCounter::colorsCount(const cv::Mat &image){
//        Image im(4096,4096);
    Image im(image.cols,image.rows);
//        Image im(image.rows,image.cols);
//        FillRandom(im);
    convertToMat(im,image);
    int cnt= (int)Count(im);
    return cnt;
}

void ColorCounter::colorsCountThread()
{
    Image im(imageThread.cols,imageThread.rows);
//        Image im(image.rows,image.cols);
//        FillRandom(im);
    convertToMat(im,imageThread);
    int cnt= (int)Count(im);

    emit finished();
    emit results(cnt);
//    QThread::currentThread()->quit();
}
